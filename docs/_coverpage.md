![logo](images/logo.053d976.png)


> BankBizNotes

* 主要记录一些银行业务笔记
* 二代支付,超级网银,农信银,网联,互金,核心,收单,信贷等

<!--
<a href='https://gitee.com/Windlisteners/bank-biz-notes/stargazers'><img src='https://gitee.com/Windlisteners/bank-biz-notes/badge/star.svg?theme=dark' alt='star'></img></a>
<a href='https://gitee.com/Windlisteners/bank-biz-notes/members'><img src='https://gitee.com/Windlisteners/bank-biz-notes/badge/fork.svg?theme=dark' alt='fork'></img></a>
-->

[![star](https://gitee.com/Windlisteners/bank-biz-notes/badge/star.svg?theme=dark)](https://gitee.com/Windlisteners/bank-biz-notes/stargazers)
[![fork](https://gitee.com/Windlisteners/bank-biz-notes/badge/fork.svg?theme=dark)](https://gitee.com/Windlisteners/bank-biz-notes/members)

<!--
[Gitee](https://gitee.com/Windlisteners/bank-biz-notes/tree/master/study-notes)
-->
[Get Started](quickstart.md)