<!--
# Kafka
- #### Kafka入门到精通
    - [为什么用nosql](kafka/kafka?id=消息队列的流派)
    - [Kafka介绍](kafka/kafka?id=一、kafka介绍)
    - [kafka基本使用](kafka/kafka?id=二、kafka基本使用)
    - [Kafka中的关键细节](kafka/kafka?id=三、kafka中的关键细节)
    - [主题、分区的概念](kafka/kafka?id=四、主题、分区的概念)
    - [Kafka集群及副本的概念](kafka/kafka?id=五、kafka集群及副本的概念)
    - [Kafka的Java客户端-生产者](kafka/kafka?id=六、kafka的java客户端-生产者)
    - [Kafka的Java客户端-消费者](kafka/kafka?id=七、消费者)
    - [Springboot中使用Kafka](kafka/kafka?id=八-、springboot中使用kafka)
    - [Kafka集群Controller、Rebalance和HW](kafka/kafka?id=九、kafka集群controller、rebalance和hw)
    - [Kafka线上问题优化](kafka/kafka?id=十、kafka线上问题优化)
    - [Kafka-eagle监控平台](kafka/kafka?id=十一、kafka-eagle监控平台)
-->